var
    // Selectors
    container,
    scenesContainer,
    sceneContainer,
    sceneElements,
    paginationContainer,

    // Unassigned values
    windowWidth,
    windowHeight,
    numberScenes,
    numberOfSceneElements,
    interval,

    // Objects
    sliderArgs,

    // Default values
    currentScene = 1,
    busy;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 James Craig - jfscraig@gmail.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @constructor
 */
var EpicSlider = function() {
    var self = this;

    /**
     * Update all global vars, calls events method and check if
     * user has passed through args
     *
     * @param args
     */
    this.init = function(args) {
        self.updateGlobals();

        if(args !== undefined) {
            self.events();
            self.setUpEpicSliderContainer(args);
            self.setUpPagination(numberScenes);
            self.setUpEpicSliderSceneContainer(args);
        }
    };

    /**
     * Handles all mouse and keyboard events for the slider
     */
    this.events = function() {
        $(document).on('click', '.pagination', function() {
            var sceneNumber = $(this).attr('data-scene-number');

            if((currentScene !== sceneNumber) && !busy) {
                $('.pagination').removeClass('active');
                $(this).addClass('active');

                clearInterval(interval);
                self.jumpToScene(sliderArgs.duration, sliderArgs.direction, sliderArgs.transition, sliderArgs.easing, sceneNumber);
            }
        });
    };

    /**
     * Updates all global vars in this file
     */
    this.updateGlobals = function() {
        container = $('#epic-slider-container');
        scenesContainer = $('#epic-slider-scenes');
        sceneContainer = $('.epic-slider-scene-container');

        windowWidth = $(window).width();
        windowHeight = $(window).height();
        numberScenes = scenesContainer.children().length;
        sceneElements = sceneContainer;
        numberOfSceneElements = sceneElements.children().length;
    };

    /**
     * Sets up additional CSS to the slider container passed
     * through by the user
     *
     * @param args
     */
    this.setUpEpicSliderContainer = function(args) {
        container.css({
            'max-width': args.maxWidth,
            'maxHeight': args.maxHeight,
            'text-align': args.textAlign,
            'vertical-align': args.verticalAlign
        });
    };

    /**
     * Sets up additional CSS to the slider scene container passed
     * through by the user
     *
     * @param args
     */
    this.setUpEpicSliderSceneContainer = function(args) {
        sceneContainer.css({
            'maxHeight': args.maxHeight,
            'text-align': args.textAlign,
            'vertical-align': args.verticalAlign
        });
    };

    /**
     * Creates pagination divs for the number of scenes
     *
     * @param numberScenes
     */
    this.setUpPagination = function(numberScenes) {
        container.append('<div id="epic-slider-pagination"></div>');
        paginationContainer = $('#epic-slider-pagination');

        for(var i = 1; i <= numberScenes; i++) {
            if(i === 1) {
                paginationContainer.append('<div class="pagination active" data-scene-number="'+ i +'">'+ i +'</div>');
            } else {
                paginationContainer.append('<div class="pagination" data-scene-number="'+ i +'">'+ i +'</div>');
            }
        }

        paginationContainer.css({
            'bottom': '-'+ (paginationContainer.height() + 10) +'px'
        });
    };

    /**
     * Called by the user to override slider default settings
     *
     * @param args
     */
    this.animateSceneElements = function(args) {
        sliderArgs = {
            duration: args.duration,
            direction: args.direction,
            transition: args.transition,
            easing: args.easing
        };

        self.loopThroughScenes(args.duration, args.direction, args.transition, args.easing);

        if(!args.loop) {
            setTimeout(function() {
                clearInterval(interval);
            }, args.duration*numberScenes);
        }
    };

    /**
     * Starts the animation loop
     *
     * @param duration
     * @param transition
     * @param easing
     * @param direction
     */
    this.loopThroughScenes = function(duration, direction, transition, easing) {
        interval = setInterval(function() {
            var pagination = $('.pagination');
            var selector = $('[data-scene="'+ currentScene +'"]');
            var size;
            var animation = {};
            var css = {};

            if(direction == 'left' || direction == 'right') {
                size = selector.width();
            } else {
                size = selector.height();
            }

            animation[direction] = size +'px';
            css[direction] = 0;
            css['display'] = 'none';

            if(!busy) {
                pagination.eq(currentScene-1).removeClass('active');

                selector.animate(animation, transition, easing, function() {
                    selector.css(css);
                    busy = false;
                });

                currentScene++;

                if(currentScene > numberScenes) {
                    currentScene = 1;
                }

                pagination.eq(currentScene-1).addClass('active');
                self.nextScene(currentScene, direction, transition, easing);
            }

            busy = true;
        }, duration);
    };

    /**
     * Goes to the scene clicked on the pagination button and restarts the
     * interval loop, calling the loopThroughScenes method
     *
     * @param duration
     * @param direction
     * @param transition
     * @param easing
     * @param nextScene
     */
    this.jumpToScene = function(duration, direction, transition, easing, nextScene) {
        var pagination = $('.pagination');
        var selector = $('[data-scene="'+ currentScene +'"]');
        var size;
        var animation = {};
        var css = {};

        if(direction == 'left' || direction == 'right') {
            size = selector.width();
        } else {
            size = selector.height();
        }

        animation[direction] = size +'px';
        css[direction] = 0;
        css['display'] = 'none';

        if(!busy) {
            pagination.eq(currentScene-1).removeClass('active');

            selector.animate(animation, transition, easing, function() {
                selector.css(css);
                busy = false;
            });

            currentScene = nextScene;

            if(nextScene > numberScenes) {
                currentScene = 1;
            }

            pagination.eq(currentScene-1).addClass('active');
            self.nextScene(currentScene, direction, transition, easing);
            self.loopThroughScenes(duration, direction, transition, easing);
        }

        busy = true;
    };

    /**
     * Reset sceneNumber to if sceneNumber increment is greater than
     * the number of scenes. Animates in next scene
     *
     * @param sceneNumber
     * @param transition
     * @param easing
     * @param direction
     */
    this.nextScene = function(sceneNumber, direction, transition, easing) {
        var selector = $('[data-scene="'+ sceneNumber +'"]');
        var size;
        var animation = {};
        var css = {};

        if(direction == 'left' || direction == 'right') {
            size = selector.width();
        } else {
            size = selector.height();
        }

        css[direction] = '-'+ size +'px';
        css['display'] = 'block';
        animation[direction] = 0;

        if(!busy) {
            busy = true;

            selector.css(css);
            selector.animate(animation, transition, easing, function() {
                busy = false;
            });
        }
    };

    this.animateInElements = function() {
        // data-duration="1000" data-easing="easeOutBack" data-slideIn="bottom" data-fadeIn=""

        $.each($('[data-scene]'), function() {

        });
    };
};